const pool = require("../config/database");
const { check, validationResult } = require('express-validator');

module.exports = {
    create: (data, callBack) => {
        
        let emailExists = false;
        let query = `select * from users where email = ?`;

        pool.query(query, [data.email],

            (error, results, fields) => {

            if (error) {
                return callBack(error);
            }

            if(JSON.stringify(results) === '[]') {
                query = `insert into users(
                                email, password, surname, 
                                first_name, gender, phone_no, 
                                user_type,address,photo_url
                            ) values(?,?,?,?,?,?,?,?,?)`;
                pool.query(query,
                    [
                        data.email,
                        data.password, 
                        data.surname,
                        data.first_name,
                        data.gender,
                        data.phone_no,
                        data.user_type,
                        data.address,
                        data.photo_url
                    ],
                    (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                    }
                );
                
                }
                else{
                    return callBack(results);
                }
            }
        );
        

    },

    getUserByUserEmail: (email, callBack) => {
        let query = `select * from users where email = ?`;
        pool.query(query, [email],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results[0]);
            }
        );
    },

    getUserByUserId: (id, callBack) => {
        let query = `select * from users where id = ?`; 
        pool.query(query, [id],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results[0]);
            }
        );
    },

    getUsers: callBack => {
        let query = `select * from users`;
        pool.query(query , [],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results);
            }
        );
    },

    updateUser: (data, callBack) => {

        let query = `update users set 
                        surname=?,first_name=?, 
                        gender=?, phone_no=?, 
                        user_type=?,address=?,about_me=?
                    where id = ?`;

        pool.query(query,
            [
                data.surname,
                data.first_name,
                data.gender,
                data.phone_no,
                data.user_type,
                data.address,
                data.about_me,
                data.id
            ],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results[0]);
            }
        );
    },
 
    deleteUser: (data, callBack) => {
        let query = `delete from users where id = ?`;
        pool.query(query ,[data.id],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results[0]);
            }
        );
    },

    updateUserPhoto: (data, callBack) => {

        let query = `update users set 
                        photo_url=?
                    where id = ?`;

        pool.query(query,
            [
                data.path,
                data.id
            ],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results[0]);
            }
        );
    }
};
  