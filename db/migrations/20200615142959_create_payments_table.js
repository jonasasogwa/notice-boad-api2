const { table } = require("../knex");

exports.up = function(knex, Promise) {
    return knex.schema.createTable('payments', function(table) {
        table.increments();
        table.integer('user_id');
        table.boolean('is_subscribed');
        table.string('amount');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('payments');
};