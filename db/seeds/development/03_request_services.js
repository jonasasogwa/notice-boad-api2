var faker = require('faker');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('request_services').del()
    .then(function () {
      // Inserts seed entries
      return knex('request_services').insert([
        {request_user_id: 1, accepting_user_id: 2, service_id: 1, request_message: faker.lorem.sentence(), accept_message: faker.lorem.sentence(), status:  faker.random.arrayElement(['Completed','Accepted','Rejected','Pending'])},
        {request_user_id: 2, accepting_user_id: 3, service_id: 2, request_message: faker.lorem.sentence(), accept_message: faker.lorem.sentence(), status:  faker.random.arrayElement(['Completed','Accepted','Rejected','Pending'])},
        {request_user_id: 3, accepting_user_id: 4, service_id: 1, request_message: faker.lorem.sentence(), accept_message: faker.lorem.sentence(), status:  faker.random.arrayElement(['Completed','Accepted','Rejected','Pending'])},
        {request_user_id: 4, accepting_user_id: 2, service_id: 2, request_message: faker.lorem.sentence(), accept_message: faker.lorem.sentence(), status:  faker.random.arrayElement(['Completed','Accepted','Rejected','Pending'])},
        {request_user_id: 1, accepting_user_id: 2, service_id: 1, request_message: faker.lorem.sentence(), accept_message: faker.lorem.sentence(), status:  faker.random.arrayElement(['Completed','Accepted','Rejected','Pending'])},
        {request_user_id: 2, accepting_user_id: 3, service_id: 2, request_message: faker.lorem.sentence(), accept_message: faker.lorem.sentence(), status:  faker.random.arrayElement(['Completed','Accepted','Rejected','Pending'])},
        {request_user_id: 3, accepting_user_id: 4, service_id: 1, request_message: faker.lorem.sentence(), accept_message: faker.lorem.sentence(), status:  faker.random.arrayElement(['Completed','Accepted','Rejected','Pending'])},
        {request_user_id: 4, accepting_user_id: 2, service_id: 2, request_message: faker.lorem.sentence(), accept_message: faker.lorem.sentence(), status:  faker.random.arrayElement(['Completed','Accepted','Rejected','Pending'])},
      ]);
    });
};
