const router = require("express").Router();
const { checkToken } = require("../auth/token_validation");
const {check} = require('express-validator')  

const { createSubscriber,
        getSubscriberByUserId,getSubscriber,
        updateSubscriber,deleteSubscriber
} = require("../controller/subscribe.controller");

router.get("/", checkToken, getSubscriber);
router.post("/create", [ 
    check("user_id", "user id is required").notEmpty(),
    check("is_subscribed", "is subscribed is required").notEmpty(),
    check("amount", "Amount is required").notEmpty()
], checkToken, createSubscriber);

router.get("/:id", checkToken, getSubscriberByUserId);

router.post("/update", [ 
    check("user_id", "user id is required").notEmpty(),
    check("is_subscribed", "is subscribed is required").notEmpty(),
    check("amount", "Amount is required").notEmpty(),
    check("updated_at", "Updated at is required").notEmpty()
], checkToken, updateSubscriber);

router.delete("/", checkToken, deleteSubscriber);
 
module.exports = router;
 