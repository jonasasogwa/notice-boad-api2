
exports.up = function(knex, Promise) {
    return knex.schema.createTable('users', function(table) {
        table.increments();
        table.string('email').notNullable();
        table.string('password').notNullable();
        table.string('surname');
        table.string('first_name').notNullable();
        table.enum('gender',['MALE','FEMALE','NONE','']).defaultTo('');
        table.string('phone_no');
        table.enum('user_type',['admin','student']).defaultTo('student');
        table.string('address');
        table.string('photo_url');
        table.string('about_me');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users');
};
