var faker = require('faker');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('posts').del()
    .then(function () {
      // Inserts seed entries
      return knex('posts').insert([
        {user_id: 1, post: faker.lorem.sentence()},
        {user_id: 2, post: faker.lorem.sentence()},
        {user_id: 3, post: faker.lorem.sentence()},
        {user_id: 4, post: faker.lorem.sentence()},
        {user_id: 5, post: faker.lorem.sentence()},
        {user_id: 6, post: faker.lorem.sentence()},
        {user_id: 9, post: faker.lorem.sentence()},
        {user_id: 7, post: faker.lorem.sentence()},
        {user_id: 1, post: faker.lorem.sentence()},
        {user_id: 2, post: faker.lorem.sentence()},
        {user_id: 3, post: faker.lorem.sentence()},
        {user_id: 4, post: faker.lorem.sentence()},
        {user_id: 5, post: faker.lorem.sentence()},
        {user_id: 6, post: faker.lorem.sentence()},
        {user_id: 9, post: faker.lorem.sentence()},
        {user_id: 7, post: faker.lorem.sentence()},
      ]);
    });
};
