const router = require("express").Router();
const { checkToken } = require("../auth/token_validation");

const { createPosts,createComments,getPostsWithUsers,getComments
} = require("../controller/postComment.controller");
const {check} = require('express-validator')  


router.post("/post", [check("user_id", "User not found").notEmpty(),
check("post", "Post can not be empty").notEmpty()
], checkToken, createPosts);
router.post("/comment", [check("user_id", "User not found").notEmpty(),
check("post_id", "Post is not found").notEmpty(),
check("comment", "Comment field can not be empty").notEmpty()
], checkToken, createComments);
router.get("/posts", checkToken, getPostsWithUsers);
router.get("/comments/:post_id", checkToken, getComments);


module.exports = router;
  