
exports.up = function(knex, Promise) {
    return knex.schema.createTable('posts', function(table) {
        table.increments();
        table.integer('user_id').notNullable();
        table.string('post').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('posts');
};