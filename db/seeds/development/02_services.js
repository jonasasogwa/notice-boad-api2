var faker = require('faker');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('services').del()
    .then(function () {
      // Inserts seed entries
      return knex('services').insert([
        {user_id: 1, service: 'Java', rating: 3},
        {user_id: 2, service: 'PHP', rating: 4},
        {user_id: 4, service: 'Angular', rating: 3},
        {user_id: 2, service: 'React', rating: 4},
        {user_id: 5, service: 'Java', rating: 3},
        {user_id: 6, service: 'PHP', rating: 4},
        {user_id: 8, service: 'Angular', rating: 3},
        {user_id: 2, service: 'Javascript', rating: 4},

        {user_id: 11, service: 'German', rating: 3},
        {user_id: 2, service: 'English', rating: 4},
        {user_id: 4, service: 'Spanish', rating: 3},
        {user_id: 2, service: 'Music', rating: 4},
        {user_id: 5, service: 'Programming', rating: 3},
        {user_id: 6, service: 'Piano', rating: 4},
        {user_id: 8, service: 'Gitta', rating: 3},
        {user_id: 2, service: 'Singing', rating: 4},
      ]);
    });
};

