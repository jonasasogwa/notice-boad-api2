const pool = require("../config/database");
const { check, validationResult } = require('express-validator');

module.exports = {

    create: (data, callBack) => {
        let query = `SELECT * FROM services WHERE user_id = ? and service = ?`;
        pool.query(query, [data.user_id, data.service],

            (error, results, fields) => {

            if (error) {
                callBack(error);
            }

            if(JSON.stringify(results) === '[]') {
                let query = `insert into services(user_id, service) values(?,?)`;
                pool.query(query,
                    [
                        data.user_id,
                        data.service
                    ],
                    (error, results, fields) => {
                        if (error) {
                            callBack(error);
                        }
                        return callBack(null, results);
                    }
                );
                
            }
                else{
                    return callBack(results);
                }
            }
        );
    },

    getServicesByUserId: (id, callBack) => {
        let query = `select * from services where user_id = ?`; 
        pool.query(query, [id],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results);
            }
        );
    },

    deleteService: (data, callBack) => {
        let query = `delete from services where id = ?`;
        pool.query(query ,[data.id],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results[0]);
            }
        );
    },

    searchServiceAndGetAllMatchedUsers: (search, callBack) => {
        let query = `SELECT u.id, u.about_me, u.created_at,  
                        u.first_name,  u.photo_url, u.surname,  
                        s.id as service_id, s.rating, s.service 
                        FROM users u INNER JOIN services s ON u.id = s.user_id where s.service = ?`;

        pool.query(query ,[search],

            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results);
            }
        );
    },

    rateUserService: (search, callBack) => {
        
        let query = `select * from services where user_id = ? AND service = ?`; 

        pool.query(query, [data.user_id, data.service, data.rating],
            (error, results, fields) => {

            if (error) {
                callBack(error);
            }
            
            let newUserRating = parseFloat(data.rating);
            let storedRating = parseFloat(JSON.stringify(results[0].rating).replace('"', '').replace('"', ''));
            
            storedRating = (newUserRating + storedRating)/2;

                query = "UPDATE services set rating = ? WHERE user_id = ? AND service = ?";
                pool.query(query,
                    [
                        data.rating,
                        data.user_id, 
                        data.service
                    ],
                    (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                    }
                );
        
            }
        );

    },


    createRequest: (data, callBack) => {

        let query = `insert into request_services(request_user_id, accepting_user_id,service_id,request_message) values(?,?,?,?)`;
        pool.query(query,
            [
                data.request_user_id,
                data.accepting_user_id,
                data.service_id,
                data.request_message
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );

    },

    getNewRequests: (search, callBack) => {
        let query = " SELECT id,request_user_id,accepting_user_id,request_message FROM request_services where  accepting_user_id = ? and (accept_message IS NULL OR accept_message = ' ') ";
        pool.query(query ,[search],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results);
            }
        );
    },


    acceptRequest: (data, callBack) => {
        query = "UPDATE request_services set accept_message = ? WHERE id = ? ";
        pool.query(query,
            [
                data.accept_message,
                data.id
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },

    getMyRequests: (search, callBack) => {
        let query = `SELECT rm.id,rm.request_user_id,rm.accepting_user_id,rm.request_message,rm.accept_message,rm.status, s.service,s.rating
                        FROM request_services rm JOIN services s ON rm.service_id = s.id where  accepting_user_id = ? and status != 'Completed' and (accept_message IS NOT NULL OR accept_message = ' ')`;
        pool.query(query ,[search],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results);
            }
        );
    },


};
  