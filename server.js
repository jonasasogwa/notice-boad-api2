const mysql = require('mysql');
require('dotenv').config();
const express = require('express');
const cors = require('cors');
const knex = require('./db/knex.js');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const port = process.env.PORT || 3000;


//Import Routes
const authRoute = require('./routes/user');
const serviceRoute = require('./routes/service');
const postCommentRoute = require('./routes/post-comment');
const subscriber = require('./routes/subscribe');

// create express app
const app = express();
app.use(cors());


// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ 
    extended: true ,
    "Content-Type":"application/x-www-form-urlencoded"
}))

// parse requests of content-type - application/json
app.use(bodyParser.json())


// define a simple route
app.use('/api/user', authRoute); 
app.use('/api/service', serviceRoute);
app.use('/api/post-comment', postCommentRoute); 
app.use('/api/subscriber', subscriber); 
app.use('/uploads',express.static(__dirname + '/uploads'));
 
// listen for requests 
app.listen(process.env.APP_PORT, () => {
    console.log("Server is listening on port 3000");
});

 