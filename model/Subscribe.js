const pool = require("../config/database");
const { check, validationResult } = require('express-validator');

module.exports = {
    
    create: (data, callBack) => {
        
        let userIdExists = false;
        let query = `select * from payments where user_id = ?`;

        pool.query(query, [data.user_id],

            (error, results, fields) => {

            if (error) {
                callBack(error);
            }

            if(JSON.stringify(results) === '[]') {
                query = `insert into payments(
                                user_id, is_subscribed, amount
                            ) values(?,?,?)`;
                pool.query(query,
                    [
                        data.user_id,
                        data.is_subscribed, 
                        data.amount
                    ],
                    (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                    }
                );
        
                }
                else{
                    return callBack(results);
                }
            }
        );
        

    },


    getSubscriberByUserId: (id, callBack) => {
        let query = `select * from payments where user_id = ?`; 
        pool.query(query, [id],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results[0]);
            }
        );
    },

    getSubscriber: callBack => {
        let query = `select * from payments`;
        pool.query(query , [],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results);
            }
        );
    },

    updateSubscriber: (data, callBack) => {

        let query = `update payments set 
                        is_subscribed=? ,amount=?, 
                        updated_at=?
                    where user_id = ?`;

        pool.query(query,
            [
                data.is_subscribed, 
                data.amount,
                data.updated_at,
                data.user_id
            ],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results[0]);
            }
        );
    },
 
    deleteSubscriber: (data, callBack) => {
        let query = `delete from payments where user_id = ?`;
        pool.query(query ,[data.id],
            (error, results, fields) => {
            if (error) {
                callBack(error);
            }
            return callBack(null, results[0]);
            }
        );
    }
};
  