const {
    create,
    getSubscriberByUserId,getSubscriber,
    updateSubscriber,deleteSubscriber
  } = require("../model/Subscribe");

const {validationResult} = require('express-validator')  
const { sign } = require("jsonwebtoken");
const pool = require("../config/database");
  
module.exports = {

    createSubscriber: (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(443).json({
                success: 0,
                message: errors.mapped()
            });
        }

        const body = req.body;
        create(body, (err, results) => {
            console.log(body)
          try{
            if (JSON.stringify((err[0].user_id))) {
                return res.json({
                    error: 0,
                    message: "The Subscriber already exist"
                });
            }    
          }catch{}
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: 0,
                    message: "Could not connect to database "
                });
            }
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: "Database connection errror"
                });
            };
            
            return res.status(200).json({
                success: 1,
                data: results
            });

        }); 
    },

    getSubscriberByUserId: (req, res) => {
        const id = req.params.id;
        getSubscriberByUserId(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record not Found"
                });
            }
            return res.json({
                    success: 1,
                    data: results
                });
            } );
    },

    getSubscriber: (req, res) => {
        getSubscriber((err, results) => {
        if (err) {
            console.log(err);
            return;
        }
        return res.json({
                success: 1,
                data: results
            });
        });
    },

    updateSubscriber: (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            /*return res.status(443).json({
                //success: 0,
                message: errors.mapped()
            });*/
            return res.json({
                message: errors.mapped()
            });
        }

        const body = req.body;
        //const salt = genSaltSync(10);
        //body.password = hashSync(body.password, salt);
        updateSubscriber(body, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "updated successfully"
            });
        });
    },

    deleteSubscriber: (req, res) => {
      const data = req.body;
      deleteSubscriber(data, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        if (!results) {
          return res.json({
            success: 0,
            message: "Record Not Found"
          });
        }
        return res.json({
          success: 1,
          message: "Subscriber deleted successfully"
        });
      });
    }
};
  