const router = require("express").Router();
const { checkToken } = require("../auth/token_validation");
const {check} = require('express-validator')  

const { createUser, login,
        getUserByUserId,getUsers,
        updateUsers,deleteUser, updateUserPhoto
} = require("../controller/user.controller");

const multer = require("multer");
var upload = multer({ dest: 'uploads' });

const { date } = require("faker");

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '_' + file.originalname)
    }
})
   
var upload = multer({ storage: storage })


router.get("/", checkToken, getUsers); 
router.post("/register", [ 
    check("first_name", "First name is required").notEmpty(),
    check("surname", "Surname is required").notEmpty(),
    check("email", "It is not a valid email").isEmail(),
    check("email", "Please provide your University of Rostock Email").contains("@uni-rostock.de"),
    check("password", "Password must be at least of 6 characters").isLength({min:6}),
    check("gender", "Gender is required").notEmpty(),
    check("phone_no", "Phone number is required ").notEmpty(),
    check("user_type", "User Type is required").notEmpty(),
    check("address", "Address is required").notEmpty(),
    check("photo_url", "Photo is required").notEmpty()
], createUser);

router.get("/:id", checkToken, getUserByUserId);
router.post("/login", login);

router.post("/update", [ 
    check("first_name", "First name is required").notEmpty(),
    check("surname", "Surname is required").notEmpty(),
    //check("password", "Password must be at least of 6 characters").isLength({min:6}),
    check("gender", "Gender is required").notEmpty(),
    check("phone_no", "Phone number is required ").notEmpty(),
    check("user_type", "User Type is required").notEmpty(),
    check("address", "Address is required").notEmpty(),
    check("photo_url", "Photo is required").notEmpty()
], checkToken, updateUsers);
 
router.delete("/", checkToken, deleteUser); 

router.post("/imageUpload/:id",[
    check("streamfile", "An image must be selected").notEmpty(),
    check("streamfile", "Only image can be selected").isMimeType()    
], upload.single('streamfile'),  updateUserPhoto); 



module.exports = router;
  