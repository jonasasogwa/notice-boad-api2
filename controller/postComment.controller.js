const {
    createPost,createComment,getPostWithUsers,getCommentsFromPost
  } = require("../model/PostComment");
const {validationResult} = require('express-validator')  
  
module.exports = {

    createPosts: (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(443).json({
                success: 0,
                message: errors.mapped()
            });
        }

        const body = req.body;
        createPost(body, (err, results) => {
            if (err) {
                return res.status(500).json({
                    success: 0,
                    message: "Could not connect to database "
                });
            }
            
            return res.status(200).json({
                success: 1,
                data: results
            });

        }); 
    },

    createComments: (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(443).json({
                success: 0,
                message: errors.mapped()
            });
        }

        const body = req.body;
        createComment(body, (err, results) => {
            if (err) {
                return res.status(500).json({
                    success: 0,
                    message: "Could not connect to database "
                });
            }
            
            return res.status(200).json({
                success: 1,
                data: results
            });

        }); 
    },

    getPostsWithUsers: (req, res) => {
        getPostWithUsers( (err, results) => {
            
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record not Found"
                });
            }
            return res.json({
                    results
                });
            }
        );
    },

    getComments: (req, res) => {
        const post_id = req.params.post_id;
        getCommentsFromPost(post_id, (err, results) => {
            
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record not Found"
                });
            }
            return res.json({
                    results
                });
            }
        );
    },
    
};