exports.up = function(knex, Promise) {
    return knex.schema.createTable('request_services', function(table) {
        table.increments();
        table.integer('request_user_id').notNullable();
        table.integer('accepting_user_id').notNullable();
        table.integer('service_id').notNullable();
        table.string('request_message');
        table.string('accept_message');
        table.enum('status',['Completed','Accepted','Rejected','Pending']).defaultTo('Pending');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('request_services');
};