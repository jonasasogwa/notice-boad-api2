const router = require("express").Router();
const { checkToken } = require("../auth/token_validation");

const { createService,getServicesByUserId,deleteService, 
    searchServiceAndGetAllMatchedUsers, rateUserService, 
    createRequest,getNewRequests, acceptRequest, getMyRequests
} = require("../controller/services.controller");
const {check} = require('express-validator')  



router.post("/create", [check("user_id", "The field is required").notEmpty(),
check("service", "This field is required").notEmpty()], checkToken, createService);
router.get("/:user_id", checkToken, getServicesByUserId);
router.delete("/", checkToken, deleteService);
router.get("/search/:search", checkToken, searchServiceAndGetAllMatchedUsers);
router.post("/rate", checkToken, rateUserService);
router.post("/search", checkToken, searchServiceAndGetAllMatchedUsers);
router.post("/request_service",checkToken, createRequest);
router.get("/new_requests/:accepting_user_id",checkToken, getNewRequests);
router.post("/accept_request", checkToken, acceptRequest);
router.get("/myrequests/:accepting_user_id",checkToken, getMyRequests);

module.exports = router;
  