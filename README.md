# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How do I get set up? ###

* clone the app
* run `npm install` to install node  dependencies
* create a db with the name `web20_db`, username as `root` and no password 
* run `knex migrate:latest` to migrate the database
* run `knex seed:run` to seed some test data to the database
* run `nodemon` to start the application 

