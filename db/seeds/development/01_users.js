var faker = require('faker');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
         // Inserts seed entries
        return knex('users').insert([
          {
            email: 'jonaschinagorom@gmail.com', 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: 'Ati.nfarahani@gmail.com', 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: 'mousavi.mozhdeh@gmail.com', 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: 'Nima.torabi@uni-rostock.de', 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: 'taimon.robinsardar@gmail.com', 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: faker.internet.email(), 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: faker.internet.email(), 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: faker.internet.email(), 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: faker.internet.email(), 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: faker.internet.email(), 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          },
          {
            email: faker.internet.email(), 
            password: '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            surname: faker.name.lastName(),
            first_name: faker.name.firstName(),
            gender: faker.random.arrayElement(['MALE','FEMALE','NONE','']),
            phone_no: faker.phone.phoneNumber(),
            user_type: faker.random.arrayElement(['admin','student']),
            address: faker.address.streetAddress(),
            photo_url: faker.image.imageUrl(),
            about_me: faker.lorem.sentences()
          }
        ]);
    });
};
