const {
    create,getUserByUserEmail,
    getUserByUserId,getUsers,
    updateUser,deleteUser, updateUserPhoto
  } = require("../model/user");

const {validationResult} = require('express-validator')  
const { hashSync, genSaltSync, compareSync } = require("bcrypt");
const { sign } = require("jsonwebtoken");
const pool = require("../config/database");

  
module.exports = {

    createUser: (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(443).json({
                success: 0,
                message: errors.mapped()
            });
        }
        
        const body = req.body;

        const salt = genSaltSync(10);
        body.password = hashSync(body.password, salt);
        create(body, (err, results) => {
            try{
                if (JSON.stringify((err[0].email))) {
                    return res.status(400).json({
                        success: 0,
                        message: "Email already exists"
                    });
                }    
            }
            catch{}
            if (err) {
                return res.status(500).json({
                    success: 0,
                    message: "Could not connect to database "
                });
            }
            else
            {
                getUserByUserEmail(body.email, (err, results1) => {
                    
                    const result = compareSync(body.password, results1.password);
                    
                    results1.password = undefined;
                    const jsontoken = sign({ result: results1 }, process.env.JWT_KEY, {
                        expiresIn: "1h"
                    });
                    return res.json({
                        success: 1,
                        message: "login successfully",
                        token: jsontoken
                    });
                        
                });

            }
        }); 
    },
//
    login: (req, res) => {
        const body = req.body;
        getUserByUserEmail(body.email, (err, results) => {
            if (err) {
                console.log(err);
            }
            if (!results) {
                return res.json({
                    success: 0,
                    data: "Invalid email or password"
                });
            }
            const result = compareSync(body.password, results.password);
            if (result) {
                results.password = undefined;
                const jsontoken = sign({ result: results }, process.env.JWT_KEY, {
                    expiresIn: "1h"
                });
                return res.json({
                    success: 1,
                    message: "login successfully",
                    token: jsontoken
                });
            } 
            else {
                return res.json({
                    success: 0,
                    data: "Invalid email or password"
                });
            }
        });   
    },

    getUserByUserId: (req, res) => {
        const id = req.params.id;
        getUserByUserId(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record not Found"
                });
            }
            results.password = undefined;
            return res.json({
                    success: 1,
                    data: results
                });
            } );
    },

    getUsers: (req, res) => {
        getUsers((err, results) => {
        if (err) {
            console.log(err);
            return;
        }
        return res.json({
                success: 1,
                data: results
            });
        });
    },

    updateUsers: (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            /*return res.status(443).json({
                //success: 0,
                message: errors.mapped()
            });*/
            return res.json({
                message: errors.mapped()
            });
        }

        const body = req.body;
        //const salt = genSaltSync(10);
        //body.password = hashSync(body.password, salt);
        updateUser(body, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "updated successfully"
            });
        });
    },

    deleteUser: (req, res) => {
      const data = req.body;
      deleteUser(data, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        if (!results) {
          return res.json({
            success: 0,
            message: "Record Not Found"
          });
        }
        return res.json({
          success: 1,
          message: "user deleted successfully"
        });
      });
    },

    updateUserPhoto: (req, res) => {
        let dataObj = {
            path: req.file.path,
            id: req.params.id
        };
        const errors = validationResult(dataObj);
        if(!errors.isEmpty()){
            return res.json({
                message: errors.mapped()
            });
        }
        updateUserPhoto(dataObj, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "updated successfully"
            });
        });
    }
};
  