var faker = require('faker');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('comments').del()
    .then(function () {
      // Inserts seed entries
      return knex('comments').insert([
        {user_id: 1, post_id: 1, comment: faker.lorem.sentence()},
        {user_id: 2, post_id: 2, comment: faker.lorem.sentence()},
        {user_id: 3, post_id: 5, comment: faker.lorem.sentence()},
        {user_id: 4, post_id: 3, comment: faker.lorem.sentence()},
        {user_id: 1, post_id: 1, comment: faker.lorem.sentence()},
        {user_id: 2, post_id: 2, comment: faker.lorem.sentence()},
        {user_id: 3, post_id: 5, comment: faker.lorem.sentence()},
        {user_id: 4, post_id: 8, comment: faker.lorem.sentence()},
        {user_id: 7, post_id: 4, comment: faker.lorem.sentence()},
        {user_id: 6, post_id: 2, comment: faker.lorem.sentence()},
        {user_id: 3, post_id: 5, comment: faker.lorem.sentence()},
        {user_id: 8, post_id: 7, comment: faker.lorem.sentence()},
        {user_id: 1, post_id: 1, comment: faker.lorem.sentence()},
        {user_id: 2, post_id: 2, comment: faker.lorem.sentence()},
        {user_id: 3, post_id: 5, comment: faker.lorem.sentence()},
        {user_id: 4, post_id: 3, comment: faker.lorem.sentence()},
        {user_id: 1, post_id: 1, comment: faker.lorem.sentence()},
        {user_id: 2, post_id: 2, comment: faker.lorem.sentence()},
        {user_id: 3, post_id: 5, comment: faker.lorem.sentence()},
        {user_id: 4, post_id: 8, comment: faker.lorem.sentence()},
        {user_id: 7, post_id: 4, comment: faker.lorem.sentence()},
        {user_id: 6, post_id: 2, comment: faker.lorem.sentence()},
        {user_id: 3, post_id: 5, comment: faker.lorem.sentence()},
        {user_id: 8, post_id: 7, comment: faker.lorem.sentence()},
      ]);
    });
};


