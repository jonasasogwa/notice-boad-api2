const pool = require("../config/database");
const { check, validationResult } = require('express-validator');

module.exports = {

    createPost: (data, callBack) => {

        let query = `insert into posts(user_id, post) values(?,?)`;
        pool.query(query,
            [
                data.user_id,
                data.post
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
        
    },

    createComment: (data, callBack) => {

        let query = `insert into comments(user_id, post_id, comment) values(?,?,?)`;
        pool.query(query,
            [
                data.user_id,
                data.post_id,
                data.comment,
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
        
    },

    getPostWithUsers: (callBack) => {
        let query = ` select  p.id, p.user_id, p.post, u.surname, u.first_name,u.photo_url
                        from posts p inner join users u on p.user_id = u.id  order by p.created_at DESC limit 10`;
        pool.query(query,
            (error, results, fields) => {
                if (error) { 
                    callBack(error);
                }
                return callBack(null, results );
            }
        );
    },

    getCommentsFromPost: (post_id,callBack) => {
        let query = ` select  c.user_id, c.post_id, c.comment, c.created_at, u.surname, u.first_name,u.photo_url
                     from comments c  inner join users u on c.user_id = u.id where c.post_id = ? order by c.created_at DESC`;
        pool.query(query,[post_id],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results );
            }
        );
    },

};
  