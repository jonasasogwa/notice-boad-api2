const {
    create,getServicesByUserId,deleteService, 
    searchServiceAndGetAllMatchedUsers, rateUserService,
    createRequest, getNewRequests,acceptRequest, getMyRequests
  } = require("../model/Services");
const {validationResult} = require('express-validator')  
  
module.exports = {

    createService: (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(443).json({
                success: 0,
                message: errors.mapped()
            });
        }

        const body = req.body;
        create(body, (err, results) => {
          try{
            if (JSON.stringify((err[0].service))) {
                return res.json({
                    error: 0,
                    message: "This Service already exist for this user"
                });
            }    
          }catch{}
            if (err) {
                return res.status(500).json({
                    success: 0,
                    message: "Could not connect to database "
                });
            }
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: "Database connection errror"
                });
            };
            
            return res.status(200).json({
                success: 1,
                data: results
            });

        }); 
    },

    getServicesByUserId: (req, res) => {
        const id = req.params.user_id;
        getServicesByUserId(id, (err, results) => {
            
        if (err) {
            console.log(err);
            return;
        }
        if (!results) {
            return res.json({
                success: 0,
                message: "Record not Found"
            });
        }
        return res.json({
                //success: 1,
                 results
            });
        });
    },

    deleteService: (req, res) => {
      const data = req.body;
      deleteService(data, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        if (!results) {
          return res.json({
            success: 0,
            message: "Record Not Found"
          });
        }
        return res.json({
          success: 1,
          message: "user deleted successfully"
        });
      });
    },
    
    searchServiceAndGetAllMatchedUsers: (req, res) => {
      const search = req.params.search;
      searchServiceAndGetAllMatchedUsers(search, (err, results) => {
        if (err) {
            console.log(err);
            return;
        }
        if (!results) {
            return res.json({
                success: 0,
                message: "Record not Found"
            });
        }
        return res.json({
                success: 1,
                data: results
            });
        });
      },



    rateUserService: (req, res) => {
      const data = req.body;
      rateUserService(data, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        if (!results) {
          return res.json({
            success: 0,
            message: "Rating wasn't updated"
          });
        }
        return res.json({
          success: 1,
          message: "Rating updated successfully"
        });
      });
    },

    createRequest: (req, res) => {
      const data = req.body;
      createRequest(data, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        if (!results) {
          return res.json({
            success: 0,
            message: "Error, Request not sent"
          });
        }
        return res.json({
          success: 1,
          message: "Request sent successfully"
        });
      });
    },

    getNewRequests: (req, res) => {
      const search = req.params.accepting_user_id;
      getNewRequests(search, (err, results) => {
          
      if (err) {
          console.log(err);
          return;
      }
      
      if (!results) {
          return res.json({
              success: 0,
              message: "Record not Found"
          });
      }
      return res.json({
              //success: 1,
               results
          });
      });
    },

    acceptRequest: (req, res) => {
      const data = req.body;
      acceptRequest(data, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        if (!results) {
          return res.json({
            success: 0,
            message: "Message not sent "
          });
        }
        return res.json({
          success: 1,
          message: "Message Sent"
        });
      });
    },
    
    getMyRequests: (req, res) => {
      const search = req.params.accepting_user_id;
      getMyRequests(search, (err, results) => {
          
      if (err) {
          console.log(err);
          return;
      }
      
      if (!results) {
          return res.json({
              success: 0,
              message: "Record not Found"
          });
      }
      return res.json({
              //success: 1,
               results
          });
      });
    },

};
  